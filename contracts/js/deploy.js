const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const { abi, bytecode } = require('./compile');

const mnemonic = 'length ceiling across heart surround differ risk eye tape ensure memory lunar';
const provider = new HDWalletProvider(mnemonic, 'http://localhost:7545');

const web3 = new Web3(provider);

console.log(web3.eth.accounts.wallet)

const deploy = async() => {
    const accounts = await web3.eth.getAccounts(console.log);

    const argumentsConstructor = [
        'Jherson',
        'Acoin',
        18,
        31000000
    ];

    const gasEstimate = await new web3.eth.Contract(abi)
    .deploy({ data: bytecode, arguments: argumentsConstructor})
    .estimateGas({ from: accounts[0] });

    const result = await new web3.eth.Contract(abi)
    .deploy({ data: bytecode, arguments: argumentsConstructor})
    .send({ gas: gasEstimate, from: accounts[0] });

    // console.log(result);
};

deploy();
